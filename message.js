const socket = io('http://localhost:3000');

const form = document.getElementById('messageForm');
const input = document.getElementById('message');
const chat = document.getElementById('chat');
const userForm = document.getElementById('userForm');
const username = document.getElementById('username');
const chatArea = document.getElementById('chatArea');
const userNameArea = document.getElementById('userNameArea');
const users = document.getElementById('users');
const nameChat = document.getElementById('nameChat');
const msg_btn = document.getElementById('msg-btn');
const usersInChat = document.getElementById("usersInChat");
let choosenChat = "all";
let user;
const allChats = {};
let usersOnline =[];
let notReadChats =[];
allChats["all"] = [];
msg_btn.addEventListener('click',(e)=>{
    if (input.value) {
        socket.emit('send message', input.value, choosenChat);
        console.log(choosenChat);
        input.value = '';
    }
});

userForm.addEventListener('submit',(e)=>{
    e.preventDefault();
    if(username.value){
        socket.emit('new user', username.value);
        user = username.value;
        chatArea.style.display = 'flex';
        userNameArea.style.display = 'none';
        socket.emit('get chat', username.value, choosenChat);
    }
})
socket.on('new message', (data) => {
    console.log(data.chat+" "+choosenChat)
    if (data.chat===choosenChat){
        socket.emit('get chat', user, choosenChat);
    }else {
        notReadChats.push(data.chat);
    }
    updateUsers(usersOnline);
});
socket.on('get user', (data) => {
    usersOnline = data;
    updateUsers(usersOnline);
    updateOnline(choosenChat);
});
function updateUsers(data){
    users.innerHTML ="";
    addChat('all');
    for(let i=0; i<data.length; i++){
        if(data[i] !== user) {
            addChat(data[i]);
        }
    }
    chat.scrollTop = chat.scrollHeight;
}
users.addEventListener('click', (e) => {
    if (e.target.classList.contains('mbr')) {
        socket.emit('get chat', username.value, e.target.dataset.name);
    }
});



socket.on('give chat',(chats,name)=>{
    choosenChat = name;
    nameChat.innerHTML = choosenChat;

    if(notReadChats.includes(choosenChat)) {
        notReadChats.splice(notReadChats.indexOf(choosenChat), 1);
    }
    chat.innerHTML = '';
    if(chats) {
        let messages = chats.messages;
        console.log(messages);
        for (let i = 0; i < messages?.length; i++) {
            if (messages[i].user === user) {
                addMyMessage(messages[i].msg);
            } else {
                addInMessage(messages[i].user, messages[i].msg);
            }
        }
        nameChat.innerHTML = choosenChat;
    }
    chat.scrollTop = chat.scrollHeight;
    updateUsers(usersOnline);
    updateOnline(choosenChat);
})
function updateOnline(name){
    usersInChat.innerHTML="";
    if(name==="all"){
        for(let i=0;i<usersOnline.length;i++){
            usersInChat.innerHTML+=`
                <div class="img m-1"> <img src="standart.png" alt="">
                                    <div class="nameUser">${usersOnline[i]}</div>
                                </div>
            `;
        }
    }else {
        usersInChat.innerHTML+=`
                <div class="img m-1"> <img src="standart.png" alt=""><div class="nameUser">${user}</div></div>
                <div class="img m-1"> <img src="standart.png" alt=""><div class="nameUser">${name}</div></div>
            `;
    }
}
function addMyMessage(mess){
    let html = `<div class="outgoing_msg">
                            <div class="sent_msg">
                                <p>${mess}</p>
                                </div>
                            </div>
                        </div>`;
    chat.innerHTML+=html;
}
function addInMessage(who, mess){
    let html = `<div class="incoming_msg">
                            <div class="incoming_msg_img"><img src="standart.png" alt="">
                                <div class="nameUser">${who}</div></div>
                            <div class="received_msg">
                                <div class="received_withd_msg">
                                    <p>${mess}</p>
                                    </div>
                                    </div>
                            </div>
                        </div>`;
    chat.innerHTML+=html;
}
function addChat(name) {
    const isActive = choosenChat === name ? 'active_chat' : '';
    const isUnread = notReadChats.includes(name) ? 'gotten' : '';
    const html = `
        <div class="chat_list mbr ${isActive}" data-name="${name}">
            <div class="chat_people mbr" data-name="${name}">
                <div class="chat_img mbr" data-name="${name}">
                    <img src="standart.png" alt="" class="mbr" data-name="${name}">
                </div>
                <div class="chat_ib mbr" data-name="${name}">
                    <h5 class="mbr" data-name="${name}">${name} <span class="chat_date"><span class="dot ${isUnread}"></span></span></h5>
                </div>
            </div>
        </div>`;
    users.innerHTML += html;
}
